# <img src="https://upload.wikimedia.org/wikipedia/en/thumb/c/ce/Spirent_logo.svg/490px-Spirent_logo.svg.png" alt="Spirent Logo" width="200"> <br/> SampleApp Deployment Using Helm

Sample application deployment package that uses Helm charts

# Use

Before installing the application you shall provision an environment using one of the available provisioning scripts:
* [Development Environment](https://gitlab.com/spirent-poc-devops/env-dev-ps)
* [Test Environment](https://gitlab.com/spirent-poc-devops/env-test-ps)
* [On-Premises Environment](https://gitlab.com/spirent-poc-devops/env-onprem-ps)
* [Cloud AWS Environment](https://gitlab.com/spirent-poc-devops/env-cloudaws-ps)

To install the application
```bash
helm install sampleapp . -n sampleapp --create-namespace
```

To upgrade the application
```bash
helm upgrade sampleapp . -n sampleapp
```

To uninstall the application
```bash
help uninstall sampleapp . -n sampleapp
kubectl delete ns sampleapp
```

# Contacts

This sample was created by *Sergey Seroukhov*